// querySelector() - method returns the first element that matches a selector
const txtFirstName = document.querySelector('#text-first-name');
const txtLastName = document.querySelector('#text-last-name')
const spanFullName = document.querySelector('#span-full-name');




// addEventListener() - method matches an event handler to an element 
// keyup - a keyboard is released after being pushed
txtFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
});

// Activity
// add addeventlistener method to the last name
txtLastName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
});


/* 
    What is event target value?
    - event.target - gives the element that triggered the event
    - event.target.value - return the element where the event occured. Gives you the value that you input on a event
*/
txtFirstName.addEventListener('keyup', (event) => {
    console.log(event.target);
    console.log(event.target.value);
    
});
